package com.lcz.ssm.exception;

public class AppException extends BaseException {

	private static final long serialVersionUID = -5888823701062368546L;

	/**
	 * @param message
	 */
	public AppException(String message) {
		super(ErrorCode.APP_FAILED, message);
	}

	/**
	 * @param cause
	 */
	public AppException(Throwable cause) {
		super(ErrorCode.APP_FAILED, cause);
	}

	/**
	 * @param message
	 * @param cause
	 */
	public AppException(String message, Throwable cause) {
		super(ErrorCode.APP_FAILED, message, cause);
	}
	
    /**
     *
     * @param errorCode
     * @param message
     * @return null
     * @throws null
     */
	public AppException(ErrorCode errorCode, String message) {
		super(errorCode, message);
	}

	/**
	 * @param errorCode
	 * @param cause
	 */
	public AppException(ErrorCode errorCode, Throwable cause) {
		super(errorCode, cause);
	}

	/**
	 * @param errorCode
	 * @param message
	 * @param cause
	 */
	public AppException(ErrorCode errorCode, String message, Throwable cause) {
		super(errorCode, message, cause);
	}

}
