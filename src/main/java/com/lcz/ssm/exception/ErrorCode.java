package com.lcz.ssm.exception;

public enum ErrorCode implements IErrorCode{
    
    /**
     * We need to group error codes by modules Prefix:
     * Module Name								Error Code Prefix
     * ----------------------------------------------------------
     * Common Error								COM_
     * AdministrationService					ADM_
     * ReportingService							RPT_
     * ExportService						    EXP_
     * ExcelUploadService						UPLOAD_
     * End of module definition
     */
	
	/****************************************************************
	 *	BASIC ERROR TYPE
	 ***************************************************************/
	
	    
	    /**
	     * SESSION 
	     */
	    SESSION_TIME_OUT,
	    /**
         * Default Error code for PersistenceException This is exception thrown
         * when an error occurs during DB operation.
         */
    	PERSISTENCE_FAILED,

        /**
         * Default Error code for BusinessException This is exception thrown
         * from business layer.
         */
    	BUSINESS_FAILED,

    	/**
         * Default Error code for PushException This is exception thrown from
         * push service.
         */
    	PUSH_MESSAGE_FAILED,

    	/**
         * Default Error code for JobExcuteException This is exception thrown
         * when an error occurs during Job execution.
         */
    	JOB_EXCUTE_FAILED,

    	/**
         * Default Error code for AppException This is exception thrown from
         * utilities methods.
         */
    	APP_FAILED,

    	/**
         * Default Error code for EmailException This is exception thrown from
         * email functional methods.
         */
    	EMAIL_FAILED,

    	/**
         * Default Error code for Exception This is exception caught from facade
         * layer.
         */
    	FATAL_INT_ERROR,

    	/***********************************************************************
         * COMMON ERROR TYPE
         **********************************************************************/
    	/**
         * can not find the user information
         */
    	COM_USER_NOT_FOUND,

    	/**
         * user do not have enough privilege to go ahead
         */
    	COM_NOT_ENOUGH_PRIVILEGE,

    	/***********************************************************************
         * ADMIN ERROR TYPE
         **********************************************************************/

    	ADM_ERROR,
    	
    	/***********************************************************************
         * REPORT ERROR TYPE
         **********************************************************************/
    	
    	/**
    	 * Default Error code for reports.
    	 */
    	RPT_ERROR,
    	/***********************************************************************
         * EXPORT/UPLOAD ERROR TYPE
         **********************************************************************/
    	
    	/**
    	 * Default Error code for upload.
    	 */
    	UPLOAD_ERROR
}
