<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://"
            + request.getServerName() + ":" + request.getServerPort()
            + path + "/";
%>
<html>
<head>
    <title>添加用户</title>
</head>
<body>
<div align="center">
    <h1>添加用户信息</h1>
</div>
<form action="<%=basePath%>user/addUser" method="post" >
        <table class="table">
            <tr>
                <td>姓名</td>
                <td><input class="form-control" type="text" name="userName"/></td>
            </tr>
            <tr>
                <td>手机号</td>
                <td><input class="form-control" type="number" name="userPhone"/></td>
            </tr>
            <tr>
                <td>邮箱</td>
                <td><input class="form-control" type="email" name="userEmail"/></td>
            </tr>
            <tr>
                <td>密码</td>
                <td><input class="form-control" type="password" name="userPwd"/></td>
            </tr>
            <tr>
                <td colspan="2" align="center"><input class="btn-primary" type="submit" value="保存"/></td>
            </tr>
        </table>
    </form>
</body>
<!--引入css脚本-->
<link rel="stylesheet" href="../../static/css/common.css" type="text/css"/>
<link href="<%=basePath%>static/bootstrap/css/bootstrap.min.css" rel="stylesheet">
<!--引入jquery脚本-->
<script src="<%=basePath%>static/jquery-easyui-1.4.2/jquery.min.js" type="text/javascript"></script>
<!--引入bootstrap脚本-->
<script src="<%=basePath%>static/bootstrap/js/bootstrap.js" type="text/javascript"></script>
</html>
