package com.lcz.ssm.model;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by LuChunzhou on 2017/6/25.
 */
public class BaseVO implements Serializable {

    protected static final long serialVersionUID = 3534485194771229942L;

    /**
     * 创建时间
     */
    protected Date createdDate = new Date();
    /**
     * 修改时间
     */
    protected Date modifiedDate = new Date();
    /**
     * 是否删除
     */
    private Integer dataStatus;

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public Date getModifiedDate() {
        return modifiedDate;
    }

    public void setModifiedDate(Date modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    public Integer getDataStatus() {
        return dataStatus;
    }

    public void setDataStatus(Integer dataStatus) {
        this.dataStatus = dataStatus;
    }

    public void setCreatedInfors(){
        this.setCreatedDate(new Date());
        this.setDataStatus(1);
    }

    public void setModifiedInfors(){
        this.setModifiedDate(new Date());
    }
}
