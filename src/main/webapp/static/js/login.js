/**
 * Created by LuChunzhou on 2017/6/27.
 */
/**
 * 获取项目根路径
 * @returns {string}
 */
function getRootPath_web() {
    //获取当前网址，如： http://localhost:8083/uimcardprj/share/meun.jsp
    var curWwwPath = window.document.location.href;
    //获取主机地址之后的目录，如： uimcardprj/share/meun.jsp
    var pathName = window.document.location.pathname;
    var pos = curWwwPath.indexOf(pathName);
    //获取主机地址，如： http://localhost:8083
    var localhostPaht = curWwwPath.substring(0, pos);
    //获取带"/"的项目名，如：/uimcardprj
    var projectName = pathName.substring(0, pathName.substr(1).indexOf('/') + 1);
    return (localhostPaht + projectName);
}

$(document).ready(function() {
    flushValidateCode();//进入页面就刷新生成验证码
});

/* 刷新生成验证码 */
function flushValidateCode(){
    var validateImgObject = document.getElementById("codeValidateImg");
    validateImgObject.src = getRootPath_web() + "/getSysManageLoginCode?time=" + new Date();
}

/**
 * 校验用户名、密码
 * @param name
 * @returns {boolean}
 */
// function checkUserName(name){
//     if(name == "" || null == name){
//         alert("用户名不能为空");
//         document.getElementById("userName").value="";
//         return false;
//     }else{
//         return true;
//     }
// }
// function checkUserPwd(password){
//     if(password == "" || null == password ){
//         alert("密码不能为空");
//         document.getElementById("userPwd").value="";
//         return false;
//     }else{
//         return true;
//     }
// }

var flag = false;

/**
 *
 * @param code
 * @returns {boolean}
 */
function checkImage(code) {
    /*校验验证码输入是否正确*/
    var url = getRootPath_web() + "/checkImageCode";
    $.get(url,{"validateCode":code},function(data){
        if(data=="ok"){
            flag = true;
        }else{
            alert("验证码错误!")
            flushValidateCode();
            flag = false;
        }
    })
}

function checkLoginMessage(){
    var name = document.getElementById("userName").value;
    var password = document.getElementById("userPwd").value;
    if(name == "" || null == name){
        alert("用户名不能为空");
        document.getElementById("userName").value="";
        document.getElementById("userPwd").value="";
        return false;
    }
    if(password == "" || null == password ){
        alert("密码不能为空");
        document.getElementById("userName").value="";
        document.getElementById("userPwd").value="";
        return false;
    }
    if(flag == false){
        alert("验证码错误！")
        return false;
    }
    else{
        return true;
    }
}