<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://"
            + request.getServerName() + ":" + request.getServerPort()
            + path + "/";
%>
<html>
<head>
    <title>登录</title>
    <style>
        /*web background*/
        .container{
            display:table;
            height:100%;
        }

        .row{
            display: table-cell;
            vertical-align: middle;
        }
        /* centered columns styles */
        .row-centered {
            text-align:center;
        }
        .col-centered {
            display:inline-block;
            float:none;
            text-align:left;
            margin-right:-4px;
        }
    </style>

</head>
<body>
<div class="container">
    <div class="row row-centered">
        <div class="well col-md-offset-3 col-md-6">
            <h2>欢迎登陆</h2>
            <form action="<%=basePath%>login/login" method="post" onsubmit="return checkLoginMessage()">
                <table class="table">
                    <tr>
                        <td>
                            <input class="form-control" id="userName" name="userName" placeholder="用户名" type="text"
                                   value="${regUser.userName}" required/>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <input class="form-control" id="userPwd" name="userPwd" placeholder="密码" type="password"
                                   value="${regUser.userPwd}" required/>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <input id="validateCode" name="validateCode" onblur="checkImage(this.value)" type="text"
                                   class="form-control" placeholder="输入验证码"/>
                            <span><img id="codeValidateImg" onClick="javascript:flushValidateCode();"/></span>
                            <p><a href="javascript:flushValidateCode();">换一张</a></p>
                        </td>
                    </tr>
                    <tr>
                        <td align="center">
                            <input class="btn-primary" type="submit" value="登录"/>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" align="center">
                            <a href="<%=basePath%>login/forgetView">忘记密码？</a>
                            还没有账号？<a href="<%=basePath%>login/registerView">去注册</a>
                        </td>
                    </tr>
                </table>
            </form>
        </div>
    </div>
</div>
</body>
<!--引入css脚本-->
<link rel="stylesheet" href="<%=basePath%>static/css/common.css" type="text/css"/>
<link href="<%=basePath%>static/bootstrap/css/bootstrap.min.css" rel="stylesheet">
<!--引入jquery脚本-->
<script src="<%=basePath%>static/jquery-easyui-1.4.2/jquery.min.js" type="text/javascript"></script>
<!--引入bootstrap脚本-->
<script src="<%=basePath%>static/bootstrap/js/bootstrap.js" type="text/javascript"></script>
<!--引入自定义js脚本-->
<script src="<%=basePath%>static/js/login.js" type="text/javascript"></script>
</html>
