package com.lcz.ssm.model;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by 巨宁 on 14-11-28.
 */
public class IsLogin {

    private Boolean isLogin = false;

    private String userName = "";

    private String image;

    private Map<String,Map<String,Boolean>> permList = new HashMap<String, Map<String, Boolean>>();

    public Map<String, Map<String, Boolean>> getPermList() {
        return permList;
    }

    public void setPermList(Map<String, Map<String, Boolean>> permList) {
        this.permList = permList;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public Boolean getIsLogin() {
        return isLogin;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public void setIsLogin(Boolean isLogin) {
        this.isLogin = isLogin;
    }

}
