package com.lcz.ssm.utils;

import java.security.MessageDigest;

/**
 * Created by LuChunzhou on 2017/6/26.
 */
public class Md5Encoder {
    public Md5Encoder() {
    }

    public static String generateCode(String source) {
        if(null == source || "".equals(source)) {
            return "";
        } else {
            byte[] sourceByte = source.getBytes();
            String target = null;
            char[] hexDigits = new char[]{'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f'};

            try {
                MessageDigest e = MessageDigest.getInstance("MD5");
                e.update(sourceByte);
                byte[] tmp = e.digest();
                char[] str = new char[32];
                int k = 0;

                for(int i = 0; i < 16; ++i) {
                    byte byte0 = tmp[i];
                    str[k++] = hexDigits[byte0 >>> 4 & 15];
                    str[k++] = hexDigits[byte0 & 15];
                }

                target = new String(str);
                return target;
            } catch (Exception var10) {
                var10.printStackTrace();
                return "";
            }
        }
    }

    public static void main(String[] args){
        System.out.println(Md5Encoder.generateCode("123"));
    }
}
