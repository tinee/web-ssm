package com.lcz.ssm.utils;

import com.alibaba.druid.util.StringUtils;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;

/**
 * Unibon
 * <p/>
 * Copyright (c) 2012 YouPeng ValueSoft Inc., All rights reserved.
 * <p/>
 * This software is the confidential and proprietary information of
 * ValueSoft Company. ("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with ValueSoft.com.
 * <p/>
 * Revision History
 * Date      		Programmer       Notes
 * 13-9-4   	     laborc		     Initial
 * *********************************************************************
 */
public class RequestUtil {
    public static Pagination buildPage(HttpServletRequest request, ModelAndView mav) {
        Pagination page = new Pagination();
        page.setPageSize(10);
        String currentPageStr = request.getParameter("currentPage");
        if (StringUtils.isEmpty(currentPageStr)) {
            page.setCurrentPage(1);
        } else {
            if (RegExpUtil.isNumeric(currentPageStr)) {
                Integer currentPage = Integer.valueOf(currentPageStr);
                page.setCurrentPage(currentPage == 0 ? 1 : currentPage);
            }
        }
        mav.addObject("pagination", page);
        return page;
    }

    public static Pagination buildPage(HttpServletRequest request) {
        Pagination page = new Pagination();
        page.setPageSize(10);
        String currentPageStr = request.getParameter("currentPage");
        if (StringUtils.isEmpty(currentPageStr)) {
            page.setCurrentPage(1);
        } else {
            if (RegExpUtil.isNumeric(currentPageStr)) {
                Integer currentPage = Integer.valueOf(currentPageStr);
                page.setCurrentPage(currentPage == 0 ? 1 : currentPage);
            }
        }
        return page;
    }

    public static Pagination buildPage(String currentPageStr) {
        Pagination page = new Pagination();
        page.setPageSize(10);
        if (StringUtils.isEmpty(currentPageStr)) {
            page.setCurrentPage(1);
        } else {
            if (RegExpUtil.isNumeric(currentPageStr)) {
                Integer currentPage = Integer.valueOf(currentPageStr);
                page.setCurrentPage(currentPage == 0 ? 1 : currentPage);
            }
        }
        return page;
    }
}
