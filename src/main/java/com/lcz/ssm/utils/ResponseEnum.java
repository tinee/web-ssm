package com.lcz.ssm.utils;

/**
 * Value
 * <p/>
 * Copyright (c) 2012 Value ValueSoft Inc., All rights reserved.
 * <p/>
 * This software is the confidential and proprietary information of
 * ValueSoft Company. ("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with ValueSoft.com.
 * <p/>
 * Revision History
 * Date      		Programmer       Notes verify VERIFY
 * 13-12-17   	     libo		     Initial
 * *********************************************************************
 */
public enum ResponseEnum {
    OK, //成功
    SUCCESS,
    VERIFY_FAIL, //验证失败
    AUTHC_FAIL, //认证失败
    AUTHC_TIMEOUT, //认证超时
    PERS_FAIL, //授权失败
    ERROR, //错误
    LOGIN_TIMEOUT //SESSION过期
}
