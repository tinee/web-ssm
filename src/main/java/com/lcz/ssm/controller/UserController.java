package com.lcz.ssm.controller;

import com.lcz.ssm.model.User;
import com.lcz.ssm.service.UserService;
import com.lcz.ssm.utils.IdGen;
import com.lcz.ssm.utils.Md5Encoder;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * Created by Zhangxq on 2016/7/15.
 */

@Controller
@RequestMapping("/user")
public class UserController{

    private Logger log = Logger.getLogger(UserController.class);
    @Resource
    private UserService userService;

    /**
     * 查询所有用户信息
     * @param request
     * @param model
     * @return
     */
    @RequestMapping(value = "/showUser", method = RequestMethod.GET)
    public String showUser(HttpServletRequest request, Model model){
        log.info("查询所有用户信息");
        List<User> userList = userService.getAllUser();
        if(userList.size()>0){
            for(User each : userList){
                each.setUserPwd("********");
            }
        }
        model.addAttribute("userList", userList);
        return "showUser";
    }

//    //或者写成下面的方法
//    @RequestMapping(value = "/showUser", method = RequestMethod.GET)
//    public ModelAndView showUser(HttpServletRequest request){
//        ModelAndView modelAndView = new ModelAndView();
//        log.info("查询所有用户信息");
//        List<User> userList = userService.getAllUser();
//        modelAndView.addObject("userList",userList);
//        modelAndView.setViewName("showUser");
//        return modelAndView;
//    }

    /**
     * 查看单个用户信息
     * @param id
     * @param request
     * @param model
     * @return
     */
    @RequestMapping(value = "/showUser/{id}", method = RequestMethod.GET)
    public String showUserById(@PathVariable String id,
                               HttpServletRequest request,
                               Model model){
        log.info("查询单个用户信息");
        User userVO = userService.getUserById(id);
        userVO.setUserPwd("********");
        model.addAttribute("user",userVO);
        return "showOneUser";
    }

    /**
     * 跳转到添加用户信息界面
     * @param request
     * @return
     */
    @RequestMapping(value = "/addUserView", method = RequestMethod.GET)
    public String addUserView(HttpServletRequest request){
        return "addUser";
    }

    /**
     * 添加用户信息
     * @param user
     * @param br
     * @param request
     * @return
     */
    @RequestMapping(value = "/addUser", method = RequestMethod.POST)
    public String addUser(User user, BindingResult br,HttpServletRequest request){ // @Validated: 对User数据进行校验
        if (br.hasErrors()) {
            return "addUser"; // 如果有错误, 则直接跳转到add添加用户页面
        }
        if(user == null
                || "".equals(user.getUserName())
                || null == user.getUserName()
                || "".equals(user.getUserPwd())
                || null == user.getUserPwd()){
            return "addUser";
        }
        List<User> userList = userService.getAllUser();
        if(userList.size()>0){
            for(User each : userList){
                if(each.getUserName().equals(user.getUserName())){
                    return "addUser";
                }
            }
        }
        user.setId(IdGen.uuid());
        user.setPwdSalt("121");
        user.setCreatedInfors();
        String password = Md5Encoder.generateCode(user.getUserPwd());
        user.setUserPwd(password);
        userService.insertUser(user);
        return "redirect:/user/showUser"; // 重定向到用户列表页面
    }

    /**
     * 修改用户信息
     * @param user
     * @param request
     * @return
     */
    @RequestMapping(value = "/updateUser", method = RequestMethod.POST)
    public String updateUserById(User user, HttpServletRequest request){
        String password = Md5Encoder.generateCode(user.getUserPwd());
        user.setUserPwd(password);
        user.setModifiedInfors();
        userService.updateUser(user);
        return "redirect:/user/showUser";
    }

    /**
     * 删除用户信息
     * @param id
     * @param request
     * @return
     */
    @RequestMapping(value = "/deleteUser/{id}", method = RequestMethod.GET)
    public String deleteUserById(@PathVariable String id,HttpServletRequest request){
        userService.deleteUserById(id);
        return "redirect:/user/showUser";
    }


}
