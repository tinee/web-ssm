package com.lcz.ssm.service.impl;

import com.lcz.ssm.dao.UserDao;
import com.lcz.ssm.model.User;
import com.lcz.ssm.service.UserService;
import com.lcz.ssm.utils.JsonUtils;
import com.lcz.ssm.utils.jedis.JedisClient;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Created by Zhangxq on 2016/7/15.
 */

@Service
@Transactional(rollbackFor = Exception.class)
public class UserServiceImpl implements UserService {
    
    @Resource
    private UserDao userDao;
    @Autowired
    private JedisClient jedisClient;

    public User getUserById(String userId) {
        //添加缓存
        try {
            //添加缓存逻辑
            //从缓存中取商品信息，商品id对应的信息
            String json = jedisClient.get("REDIS_ITEM_KEY" + ":" + userId + ":desc");
            //判断是否有值
            if (!StringUtils.isBlank(json)) {
                //把json转换成java对象
                User jedisUser = JsonUtils.jsonToPojo(json, User.class);
                return jedisUser;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        //创建查询条件
        User u = userDao.selectUserById(userId);

        try {
            //把商品信息写入缓存
            jedisClient.set("REDIS_ITEM_KEY" + ":" + userId + ":desc", JsonUtils.objectToJson(u));
            //设置key的有效期
            jedisClient.expire("REDIS_ITEM_KEY" + ":" + userId + ":desc", 86400);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return u;
    }

    public void insertUser(User user) {
        if(null != user){
            userDao.insertUser(user);
        }
    }

    public void updateUser(User user) {
        if(null != user){
            userDao.updateUser(user);
        }
    }

    public void deleteUserById(String id) {
        if(null != id){
            userDao.deleteUserById(id);
        }
    }

    @Override
    public Set<String> findUserRoles(String userName) {
        Set<String> stringSet = null;
        if (null != userName) {
            stringSet = userDao.findUserRoles(userName);
        }
        return stringSet;
    }

    @Override
    public Set<String> findUserPermissions(String userName) {
        Set<String> stringSet = null;
        if (null != userName) {
            stringSet = userDao.findUserPermissions(userName);
        }
        return stringSet;
    }

    @Override
    public User queryUserByLoginNamePasswordMap(Map<String, Object> params) {
        User user = new User();
        if(!params.isEmpty()){
            user = userDao.queryUserByLoginNamePasswordMap(params);
        }
        return user;
    }

    @Override
    public void regUser(User user) {
        userDao.insertUser(user);
    }

    public List<User> getAllUser() {
        return userDao.selectAllUser();
    }

    public User queryUserByName(String userName) {
        return userDao.queryUserByName(userName);
    }
}
