/*
Navicat MySQL Data Transfer

Source Server         : localhost_3306
Source Server Version : 50624
Source Host           : localhost:3306
Source Database       : test

Target Server Type    : MYSQL
Target Server Version : 50624
File Encoding         : 65001

Date: 2017-06-25 19:12:09
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for t_user
-- ----------------------------
DROP TABLE IF EXISTS `t_user`;
CREATE TABLE `t_user` (
  `id` varchar(100) NOT NULL COMMENT 'id',
  `user_name` varchar(100) NOT NULL COMMENT '姓名',
  `user_phone` varchar(100) DEFAULT NULL COMMENT '电话',
  `user_email` varchar(100) DEFAULT NULL COMMENT '邮箱',
  `pwd_salt` varchar(100) DEFAULT NULL COMMENT '盐',
  `user_pwd` varchar(100) NOT NULL COMMENT '密码',
  `created_date` datetime DEFAULT NULL,
  `modified_date` datetime DEFAULT NULL,
  `data_status` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of t_user
-- ----------------------------
INSERT INTO `t_user` VALUES ('1', 'tom3', '13357170390', '109@qq.com', '112q-12wq', '123', '2017-06-23 22:00:40', null, '0');
INSERT INTO `t_user` VALUES ('2', '小明', '13577675588', '2227228@qq.com', '33e2q-1dsa', '123', '2017-06-23 22:00:40', '2017-06-23 23:36:30', '0');
