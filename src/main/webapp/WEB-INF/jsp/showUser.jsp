<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://"
            + request.getServerName() + ":" + request.getServerPort()
            + path + "/";
%>
<html>
<head>
    <title>用户信息列表</title>
</head>
<body>
<c:if test="${empty sessionScope.loginUser}">
    <table class="table">
        <div align="center">
            <h4>您当前未登录，点击【<a href="<%=basePath%>login/loginView">这里</a>】登录。</h4>
        </div>
    </table>
</c:if>
<c:if test="${! empty sessionScope.loginUser && !empty userList}">
    <table class="table">
        <div align="center">
            <h1>登录成功!</h1>
        </div>
        <div>
            <h3 class="pull-left">欢迎您,【${sessionScope.loginUser.userName}】</h3>
            <button class="btn btn-primary pull-right" type="button" onclick="loginOut()">退出</button>
        </div>
    </table>
    <table class="table">
        <tr>
            <td colspan="4">
                <button class="btn btn-primary pull-right" type="button" onclick="addUser()">添加用户</button>
            </td>
        </tr>
        <tr>
            <td>姓名</td>
            <td>手机号</td>
            <td colspan="2">操作</td>
        </tr>
        <c:forEach var="user" items="${userList}">
            <tr>
                <td>${user.userName}</td>
                <td>${user.userPhone}</td>
                <td><button class="btn btn-primary pull-right" type="button" onclick="toOneUser('${user.id}')">编辑</button></td>
                <td><button class="btn btn-danger pull-right" type="button" onclick="isDelete('${user.id}','${user.userName}')">删除</button></td>
            </tr>
        </c:forEach>
    </table>
</c:if>
</body>
<!--引入css脚本-->
<link rel="stylesheet" href="<%=basePath%>static/css/common.css" type="text/css"/>
<link href="<%=basePath%>static/bootstrap/css/bootstrap.min.css" rel="stylesheet">
<!--引入jquery脚本-->
<script src="<%=basePath%>static/jquery-easyui-1.4.2/jquery.min.js" type="text/javascript"></script>
<!--引入bootstrap脚本-->
<script src="<%=basePath%>static/bootstrap/js/bootstrap.js" type="text/javascript"></script>
<!--自定义js-->
<script  src="<%=basePath%>static/js/showUser.js"type="text/javascript"></script>
</html>