/**
 * Created by LuChunzhou on 2017/6/27.
 */
/**
 * 获取项目根路径
 * @returns {string}
 */
function getRootPath_web() {
    //获取当前网址，如： http://localhost:8083/uimcardprj/share/meun.jsp
    var curWwwPath = window.document.location.href;
    //获取主机地址之后的目录，如： uimcardprj/share/meun.jsp
    var pathName = window.document.location.pathname;
    var pos = curWwwPath.indexOf(pathName);
    //获取主机地址，如： http://localhost:8083
    var localhostPaht = curWwwPath.substring(0, pos);
    //获取带"/"的项目名，如：/uimcardprj
    var projectName = pathName.substring(0, pathName.substr(1).indexOf('/') + 1);
    return (localhostPaht + projectName);
}

function loginOut(){
    var flag = confirm("确定退出吗?");

    if(flag){
        window.location.href = getRootPath_web() + "/login/loginOut";
    }
}
function addUser(){
    window.location.href = getRootPath_web() + "/user/addUserView";
}
function toOneUser(id){
    window.location.href = getRootPath_web() + "/user/showUser/"+id;
}
function isDelete(id,name){
    //confirm方法弹出一个对话框,可以选择确定与取消操作
    //同时该方法有返回值,true和false,两个布尔值
    var flag = confirm("确定删除"+name+"吗?");
    if(flag){
        window.location.href= getRootPath_web() + "/user/deleteUser/"+id;
    }
}