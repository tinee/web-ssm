package com.lcz.ssm.utils;

import com.lcz.ssm.exception.BusinessException;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PropertiesLoaderUtils;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.Properties;

/**
 * Value
 * <p/>
 * Copyright (c) 2012 Value ValueSoft Inc., All rights reserved.
 * <p/>
 * This software is the confidential and proprietary information of
 * ValueSoft Company. ("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with ValueSoft.com.
 * <p/>
 * Revision History
 * Date      		Programmer       Notes
 * 13-12-17   	     libo		     Initial
 * *********************************************************************
 */
@Component
public class ValidationMessage implements InitializingBean {
    private static Properties props = null;
    public static String getMessage(String code){
        if(props==null){
            try{
                ValidationMessage.reloadMessages();
            }catch (Exception e){
                e.printStackTrace();
            }

        }
        if (props.containsKey(code)){
            return props.get(code).toString();
        }else {
            throw new BusinessException(code+"不存在");
        }
    }
    public static void reloadMessages() throws IOException {
        Resource resource = new ClassPathResource("/ValidationMessages.properties");
        ValidationMessage.props = PropertiesLoaderUtils.loadProperties(resource);
    }
    @Override
    public void afterPropertiesSet() throws Exception {
        ValidationMessage.reloadMessages();
    }
}
