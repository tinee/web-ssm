package com.lcz.ssm.dao;

import com.lcz.ssm.model.User;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Created by Zhangxq on 2016/7/15.
 */

@Repository
public interface UserDao {

    User selectUserById(@Param("userId") String userId);

    User queryUserByUserNameAndPassword(@Param("userName") String userName, @Param("userPwd") String userPwd);

    List<User> selectAllUser();

    void insertUser(User user);

    void updateUser(User user);

    void deleteUserById(String id);

    User queryUserByName(String userName);

    Set<String> findUserRoles(String userName);

    Set<String> findUserPermissions(String userName);

    User queryUserByLoginNamePasswordMap(Map<String, Object> params);

    List<User> queryUserForPage(User user);

}
