package com.lcz.ssm.utils;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;


public class DateUtil {
	/**
	 * 根据formartStr 把String 转换为Date
	 * @param dateStr
	 * @param formatStr
	 * @return
	 */
	public static Date stringToDate(String dateStr,String formatStr){
		if(dateStr==null||"".equals(dateStr)) return null;
		DateFormat format= new SimpleDateFormat(formatStr); 
		Date date = null;
		try {
			date = format.parse(dateStr);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return date;
	}
	public static String dateToString(Date date,String formatStr){
		if(date==null) return null;
		try {
			DateFormat format= new SimpleDateFormat(formatStr); 
			return format.format(date);
		} catch (Exception e) {
            e.getMessage();
		}
		return null;
	}

	public static String getDateUUid(){
		DateFormat format= new SimpleDateFormat("yyyyMMddHHmmss"); 
		try {
			return format.format(new Date());
		} catch (Exception e) {
            e.getMessage();
		}
		return null;
	}
	
	public static String dateAdd(String baseDate,int addNum) throws ParseException{
		if(baseDate!=null&&!"".equals(baseDate)){
			SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
			Date dt=sdf.parse(baseDate);
			Calendar cal = Calendar.getInstance();
			cal.setTime(dt);
			cal.add(Calendar.DAY_OF_MONTH, addNum);
			return sdf.format(cal.getTime());
		}
		return null;
	}
}
