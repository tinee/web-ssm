package com.lcz.ssm.model;

import org.hibernate.validator.constraints.NotEmpty;

/**
 * Value
 * <p/>
 * Copyright (c) 2012 Value ValueSoft Inc., All rights reserved.
 * <p/>
 * This software is the confidential and proprietary information of
 * ValueSoft Company. ("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with ValueSoft.com.
 * <p/>
 * Revision History
 * Date      		Programmer       Notes
 * 13-12-10   	     libo		     Initial
 * *********************************************************************
 */
public class LoginForm {
    /**
     * 用户名
     */
    @NotEmpty(message="用户名不能为空")
    public String userName;

    /**
     * 密码
     */
    @NotEmpty(message="密码不能为空")
    public String userPwd;

    /**
     * 验证码
     */
    //@NotEmpty(message="验证码不能为空")
    public String rand;

    /**
     *跳转类型
     */
    public String redirectURL;

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserPwd() {
        return userPwd;
    }

    public void setUserPwd(String userPwd) {
        this.userPwd = userPwd;
    }

    public String getRand() {
        return rand;
    }

    public void setRand(String rand) {
        this.rand = rand;
    }

    public String getRedirectURL() {
        return redirectURL;
    }

    public void setRedirectURL(String redirectURL) {
        this.redirectURL = redirectURL;
    }
}
