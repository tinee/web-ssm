package com.lcz.ssm.controller;

import java.util.HashSet;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import redis.clients.jedis.HostAndPort;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisCluster;
import redis.clients.jedis.JedisPool;

/**
 * Created by LuChunzhou on 2017/7/29.
 * redis测试
 */
public class JedisTest {
    private static final Logger LOGGER = LoggerFactory.getLogger(JedisTest.class);


    /*********************单机版测试start*****************************/

    @Test
    public void testJedisSingle() {
        //创建一个jedis的对象。
        Jedis jedis = new Jedis("192.168.147.137", 6379);
        //调用jedis对象的方法，方法名称和redis的命令一致。
        jedis.set("key1", "jedis test");
        String string = jedis.get("key1");
        System.out.println(string);
        //关闭jedis。
        jedis.close();
    }

    /**
     * 使用连接池
     */
    @Test
    public void testJedisPool() {
        //创建jedis连接池
        JedisPool pool = new JedisPool("192.168.147.137", 6379);
        //从连接池中获得Jedis对象
        Jedis jedis = pool.getResource();
        String string = jedis.get("key1");
        System.out.println(string);
        //关闭jedis对象
        jedis.close();
        pool.close();
    }

    /*********************单机版测试end*****************************/

    /*********************集群版测试start*****************************/

    /**
     * <p>Title: testJedisCluster</p>
     * <p>Description: </p>
     */
//    @Test
//    public void testJedisCluster() {
//        LOGGER.debug("调用redisCluster开始");
//        HashSet<HostAndPort> nodes = new HashSet<HostAndPort>();
//        nodes.add(new HostAndPort("192.168.147.137", 7001));
//        nodes.add(new HostAndPort("192.168.147.137", 7002));
//        nodes.add(new HostAndPort("192.168.147.137", 7003));
//        nodes.add(new HostAndPort("192.168.147.137", 7004));
//        nodes.add(new HostAndPort("192.168.147.137", 7005));
//        nodes.add(new HostAndPort("192.168.147.137", 7006));
//        LOGGER.info("创建一个JedisCluster对象");
//        JedisCluster cluster = new JedisCluster(nodes);
//        LOGGER.debug("设置key1的值为1000");
//        cluster.set("key1", "1000");
//
//        LOGGER.debug("从Redis中取key1的值");
//        String string = cluster.get("key1");
//        System.out.println("从Redis中取key1的值:"+string);
//        cluster.close();
//        //测试异常
////        try {
////            int a = 1/0;
////        } catch (Exception e) {
////            LOGGER.error("系统发送异常", e);
////        }
//    }

    /*********************集群版测试end*****************************/

    /*********************整合spring测试start*****************************/

    /**
     * 单机版测试
     * <p>Title: testSpringJedisSingle</p>
     * <p>Description: </p>
     */
    @Test
    public void testSpringJedisSingle() {
        ApplicationContext applicationContext = new ClassPathXmlApplicationContext("classpath:spring-redis.xml");
        JedisPool pool = (JedisPool) applicationContext.getBean("redisClient");
        Jedis jedis = pool.getResource();
        String string = jedis.get("key1");
        System.out.println(string);
        jedis.close();
        pool.close();
    }

//    /**
//     * 集群版测试
//     */
//    @Test
//    public void testSpringJedisCluster() {
//        ApplicationContext applicationContext = new ClassPathXmlApplicationContext("classpath:spring-redis.xml");
//        JedisCluster jedisCluster =  (JedisCluster) applicationContext.getBean("redisClient");
//        String string = jedisCluster.get("key1");
//        System.out.println(string);
//        jedisCluster.close();
//    }

    /*********************整合spring测试end*****************************/
}
