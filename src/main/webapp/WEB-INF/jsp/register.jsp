<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://"
            + request.getServerName() + ":" + request.getServerPort()
            + path + "/";
%>
<html>
<head>
    <title>注册用户</title>
</head>
<body>

    <form action="<%=basePath%>login/register" method="post" onsubmit="return checkLoginMessage()">
        <div align="center">
            <h1>注册</h1>
        </div>
        <table class="table">
            <tr>
                <td><input class="form-control" id="userName" placeholder="用户名" type="text" name="userName" autocomplete="off"></td>
            </tr>
            <tr>
                <td><input class="form-control" id="userPwd" placeholder="密码" type="text" name="userPwd" autocomplete="off" onfocus="this.type='password'"></td>
            </tr>
            <tr>
                <td>
                    <input id="validateCode" name="validateCode" onblur="checkImage(this.value)" type="text" class="form-control" placeholder="输入验证码"/>
                    <span><img id="codeValidateImg" onClick="javascript:flushValidateCode();"/></span>
                    <p><a href="javascript:flushValidateCode();" >换一张</a></p>
                </td>
            </tr>
            <tr>
                <td colspan="2" align="center"><input class="btn-primary" type="submit" value="提交"></td>
            </tr>
        </table>
    </form>
</body>
<!--引入css脚本-->
<link rel="stylesheet" href="../../static/css/common.css" type="text/css"/>
<link href="<%=basePath%>static/bootstrap/css/bootstrap.min.css" rel="stylesheet">
<!--引入jquery脚本-->
<script src="<%=basePath%>static/jquery-easyui-1.4.2/jquery.min.js" type="text/javascript"></script>
<!--引入bootstrap脚本-->
<script src="<%=basePath%>static/bootstrap/js/bootstrap.js" type="text/javascript"></script>
<!--引入自定义js脚本-->
<script src="<%=basePath%>static/js/register.js" type="text/javascript"></script>
</html>
