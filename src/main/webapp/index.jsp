<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://"
            + request.getServerName() + ":" + request.getServerPort()
            + path + "/";
%>
<html>
<head>
    <title>欢迎页</title>
</head>
<body>
    <div style="margin-top: 150px;" align="center">
        <h1>用户信息管理系统 v1</h1>
        <button class="btn btn-primary" type="button" style="margin-top: 30px;" onclick="toLogin()">点击进入</button>
    </div>
</body>
<!--引入css脚本-->
<link href="<%=basePath%>static/bootstrap/css/bootstrap.min.css" rel="stylesheet">
<!--引入jquery脚本-->
<script src="<%=basePath%>static/jquery-easyui-1.4.2/jquery.min.js" type="text/javascript"></script>
<!--引入bootstrap脚本-->
<script src="<%=basePath%>static/bootstrap/js/bootstrap.js" type="text/javascript"></script>
<!--自定义js-->
<script  src="<%=basePath%>static/js/index.js"type="text/javascript"></script>
</html>
