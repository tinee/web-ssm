package com.lcz.ssm.exception;

import java.util.HashMap;
import java.util.Map;

public abstract class BaseException extends RuntimeException {

    private ErrorCode errorCode;
    private Map<String, Object> params = new HashMap<String, Object>();

    /**
     * @param errorCode
     * @param message
     */
    public BaseException(ErrorCode errorCode, String message) {
        super(message);
        this.errorCode = errorCode;
    }

    /**
     * @param errorCode
     * @param cause
     */
    public BaseException(ErrorCode errorCode, Throwable cause) {
        super(cause);
        this.errorCode = errorCode;
    }

    /**
     * @param errorCode
     * @param message
     * @param cause
     */
    public BaseException(ErrorCode errorCode, String message, Throwable cause) {
        super(message, cause);
        this.errorCode = errorCode;
    }

    /**
     * @return
     */
    public String getErrorCode() {
        return errorCode.toString();
    }

    /**
     * @param errorCode
     */
    public void setErrorCode(ErrorCode errorCode) {
        this.errorCode = errorCode;
    }

    public Map<String, Object> getParams() {
        return params;
    }

    public void setParams(Map<String, Object> params) {
        this.params = params;
    }

    public BaseException putParam(String key, Object value) {
        params.put(key, value);
        return this;
    }

    /*
         * @Override
         * (non-Javadoc)
         * @see java.lang.Throwable#toString()
         */
    public String toString() {
        return "ErrorCode=" + errorCode + ", class=" + super.toString();
    }

}
