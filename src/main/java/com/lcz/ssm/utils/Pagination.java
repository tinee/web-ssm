package com.lcz.ssm.utils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by LuChunzhou on 2017/6/30.
 */
public class Pagination {
    private Long count = Long.valueOf(0L);
    private int start = 0;
    private int totalPages = 0;
    private int currentPage = 1;
    private int pageSize = 15;
    private boolean nextPage;
    private boolean previousPage;
    private String otherParams;
    private List<?> results;
    private Map<String, Object> params = new HashMap();

    public Pagination() {
    }

    public Long getCount() {
        return this.count;
    }

    public void setCount(int count) {
        this.setCount(Long.valueOf((long)count));
    }

    public void setCount(Long count) {
        this.count = count;
        if(this.count.longValue() == 0L) {
            this.totalPages = 1;
        } else if(this.count.longValue() % (long)this.pageSize == 0L) {
            this.totalPages = (int)(this.count.longValue() / (long)this.pageSize);
        } else {
            this.totalPages = (int)(this.count.longValue() / (long)this.pageSize + 1L);
        }

        if(this.currentPage == 1) {
            this.previousPage = false;
        } else {
            this.previousPage = true;
        }

        if(this.currentPage == this.totalPages) {
            this.nextPage = false;
        } else {
            this.nextPage = true;
        }

        this.start = (this.currentPage - 1) * this.pageSize;
    }

    public int getStart() {
        return this.start;
    }

    public void setStart(int start) {
        this.start = start;
    }

    public int getTotalPages() {
        return this.totalPages;
    }

    public void setTotalPages(int totalPages) {
        this.totalPages = totalPages;
    }

    public int getCurrentPage() {
        return this.currentPage;
    }

    public void setCurrentPage(int currentPage) {
        this.currentPage = currentPage;
    }

    public int getPageSize() {
        return this.pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    public Map<String, Object> getParams() {
        return this.params;
    }

    public void setParams(Map<String, Object> params) {
        this.params = params;
    }

    public boolean isNextPage() {
        return this.nextPage;
    }

    public void setNextPage(boolean nextPage) {
        this.nextPage = nextPage;
    }

    public boolean isPreviousPage() {
        return this.previousPage;
    }

    public void setPreviousPage(boolean previousPage) {
        this.previousPage = previousPage;
    }

    public String getOtherParams() {
        return this.otherParams;
    }

    public void setOtherParams(String otherParams) {
        this.otherParams = otherParams;
    }

    public List<?> getResults() {
        return this.results;
    }

    public void setResults(List<?> results) {
        this.results = results;
    }
}
