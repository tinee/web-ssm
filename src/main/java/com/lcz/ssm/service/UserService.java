package com.lcz.ssm.service;

import com.lcz.ssm.model.User;

import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Created by Zhangxq on 2016/7/15.
 */
public interface UserService {

    List<User> getAllUser();

    User queryUserByName(String userName);

    User getUserById(String userId);

    void insertUser(User user);

    void updateUser(User user);

    void deleteUserById(String id);

    Set<String> findUserRoles(String userName);

    Set<String> findUserPermissions(String userName);

    User queryUserByLoginNamePasswordMap(Map<String, Object> params);

    void regUser(User user);
}
