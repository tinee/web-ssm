package com.lcz.ssm.exception;

public class BusinessException extends BaseException {

	private static final long serialVersionUID = 457855784821577921L;

	/**
	 * @param message
	 */
	public BusinessException(String message) {
		super(ErrorCode.BUSINESS_FAILED, message);
	}

	/**
	 * @param cause
	 */
	public BusinessException(Throwable cause) {
		super(ErrorCode.BUSINESS_FAILED, cause);
	}

	/**
	 * @param message
	 * @param cause
	 */
	public BusinessException(String message, Throwable cause) {
		super(ErrorCode.BUSINESS_FAILED, message, cause);
	}
	
	/**
	 * @param errorCode
	 * @param message
	 */
	public BusinessException(ErrorCode errorCode, String message) {
		super(errorCode, message);
	}

	/**
	 * @param errorCode
	 * @param cause
	 */
	public BusinessException(ErrorCode errorCode, Throwable cause) {
		super(errorCode, cause);
	}

	/**
	 * @param errorCode
	 * @param message
	 * @param cause
	 */
	public BusinessException(ErrorCode errorCode, String message, Throwable cause) {
		super(errorCode, message, cause);
	}

}
